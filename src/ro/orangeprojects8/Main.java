package ro.orangeprojects8;

public class Main {

    public static void main(String[] args) {

        String[] receptionist = new String[5];
        int[] etaj = new int[5];

        receptionist[0] = "John Snow";
        receptionist[1] = "William Cooper";
        receptionist[2] = "Patricia Johnson";
        receptionist[3] = "Kate Anderson";
        receptionist[4] = "Dustin Rhodes";

        etaj[0] = 1;
        etaj[1] = 2;
        etaj[2] = 3;
        etaj[3] = 4;
        etaj[4] = 5;

        System.out.println("The receptionist on the " + etaj[0] + " -st floor is: " + receptionist[0]);
        System.out.println("The receptionist on the " + etaj[1] + " -nd floor is: " + receptionist[1]);
        System.out.println("The receptionist on the " + etaj[2] + " -rd floor is: " + receptionist[2]);
        System.out.println("The receptionist on the " + etaj[3] + " -th floor is: " + receptionist[3]);
        System.out.println("The receptionist on the " + etaj[4] + " -th floor is: " + receptionist[4]);

        String[][] proprietar = new String[2][2];
        proprietar[0][0] = "Mr.";
        proprietar[0][1] = "Mrs.";
        proprietar[1][0] = "Smith Riley";
        proprietar[1][1] = "Catherine Jade";

        System.out.println(proprietar[0][0] + proprietar[1][0] + " and " + proprietar[0][1] + proprietar [1][1] +
                " are the owners of Plaza Hotel" );


        char[] security = new char[]{'B','E','D','O','N','A','L','D','A','T','E'};
        char[] securityHotel = new char[6];

        System.arraycopy(security,2,securityHotel,0,6);

        String b = new String(securityHotel); // convert char array to string

        System.out.println("The hotel's security is provided by: " + b);





    }
}
